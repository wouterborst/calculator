export default {
    buttons: {
        calculate: 'Calculate',
    },
    mortageCalculator: {
        title: 'Mortage calculator',
        propertyPurchasePrice: 'Property purchase price',
        totalSavings: 'Total savings',
        realEstateCommission: 'Real estate commission',
        annualRepaymentRate: 'Annual repayment rate(%)',
        errors: {
            emptyRequiredFields: 'Some required fields are empty',
        },
        widgets: {
            loanToValue: 'Loan to value',
            impliedLoan: 'Implied loan',
        }
    },
    mortageTable: {
        years: 'years',
        ficationLength: 'Fication length',
        monthlyRate: 'Monthly rate',
        interestRate: 'interest rate'
    }
}