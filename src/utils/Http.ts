import { API_ENDPOINT } from "@/constants";

export default class Http {
    /**
     * Post
     * @param query 
     * @returns response
     */
    static post(query) {
        return fetch(API_ENDPOINT, {
            method: 'POST',
                body: JSON.stringify({
                query: query,
            }),
            headers: {
                'accept': "*/*",
                'accept-language': 'en,de;q=0.9,cs;q=0.8',
                'cache-control': 'no-cache',
                'content-type': 'application/json',
                'pragma': 'no-cache',
                'sec-fetch-dest': 'empty',
                'sec-fetch-mode': 'cors',
                'sec-fetch-site': 'same-origin',
            }
            }).then(async (data) => {
                if(data.status === 200) {
                    const result = await data.json();
                    return result.data.root.ratesTable;
                }
            }).catch(error => {
                throw error;
            }); 
    }
}