import { createApp } from 'vue';
import App from './App.vue';

import i18n from './locale';
import vuetify from './plugins/vuetify';

import './assets/main.css';

createApp(App)
.use(vuetify)
.use(i18n)
.mount('#app');